const express = require("express");
const path = require("path");

const app = express();

const http = require("http").Server(app);
const PORT = process.env.PORT || 8080;

// Attach HTTP server to Socket.io
const io = require("socket.io")(http);

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "src/index.html"));
});

// Create a new connection to Socket.io
io.on("connection", (socket) => {
  console.log(socket.id);
  console.log("user connected!");

  socket.on("disconnect", () => {
    console.log("user disconnected!");
  });

  socket.on("message", (msg) => {
    console.log("Sent message: ", msg);
  });

  // emit event with server id
  socket.emit("server", "Sent Data from Server!");
  socket.emit("server2", "Sent Data from Server2!");

});

http.listen(PORT, () => {
  console.log(`App http listenong at port: http://localhost:${PORT}`);
});
